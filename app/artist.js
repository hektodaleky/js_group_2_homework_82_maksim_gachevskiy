const express = require('express');
const Artist = require('../models/Artist');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null, config.uploadPath);
    }, filename: (req, file, cd) => {
        cd(null, nanoid() + path.extname(file.originalname))
    }
});


const upload = multer({storage});

const createRouter = () => {
    router.get('/', (req, res) => {

        Artist.find()
            .then(artist => res.send(artist))
            .catch(error => res.sendStatus(500));
    });

    router.post('/', upload.single('image'), (req, res) => {
        const artist = new Artist(req.body);
        if (req.file) {
            artist.image = req.file.filename;
        }


        artist.save()
            .then(artist => res.send(artist))
            .catch(err => res.status(400).send(err));
    });
    return router;
};

module.exports = createRouter;