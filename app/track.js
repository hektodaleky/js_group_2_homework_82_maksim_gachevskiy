const express = require('express');
const Track = require('../models/Track');

const router = express.Router();

const createRouter = () => {
    /*Есть возможность вывожить все песни автора*/

    router.get('/', (req, res) => {
        Track.find().populate({path: 'album', populate: {path: 'author', select: 'name'}})
            .then(track => req.query.album ?
                res.send(track.filter(item => item.album.name.toLowerCase() === req.query.album.toLowerCase()))
                : (req.query.artist ?
                    res.send(track.filter(item => item.album.author.name.toLowerCase() === req.query.artist.toLowerCase()))
                    : res.send(track)))
            .catch(() => res.sendStatus(500))
    });


    router.post('/', (req, res) => {
        const track = new Track(req.body);
        track.save()
            .then(track => res.send(track))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;