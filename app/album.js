const express = require('express');
const Album = require('../models/Album');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null, config.uploadPath);
    }, filename: (req, file, cd) => {
        cd(null, nanoid() + path.extname(file.originalname))
    }
});


const upload = multer({storage});

const createRouter = () => {
    router.get('/', (req, res) => {
        Album.find().populate('author')
            .then(albums => req.query.artist ?
                res.send(albums.filter(item => item.author.name.toLowerCase() === req.query.artist.toLowerCase()))
                : res.send(albums))
            .catch(error => {
                console.log(error);
                res.sendStatus(500)
            });
    });

    router.post('/', upload.single('image'), (req, res) => {
        const album = new Album(req.body);
        if (req.file) {
            album.image = req.file.filename;
        }


        album.save()
            .then(album => res.send(album))
            .catch(err => res.status(400).send(err));
    });
    return router;
};

module.exports = createRouter;